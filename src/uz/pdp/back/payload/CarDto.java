package uz.pdp.back.payload;

import uz.pdp.back.model.enums.CarType;

import java.util.UUID;

public record CarDto(String name, UUID driverId, String number, CarType type) {
}
