package uz.pdp.back.model;

import java.io.Serializable;

public class Order implements Serializable {
    private long id;
    private Location from;
    private Location to;
    private Car car;
    private User client;
    private final Long startTime = System.currentTimeMillis();
    private Long endTime;
    private Long amount;

}
