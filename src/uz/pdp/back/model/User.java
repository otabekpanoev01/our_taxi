package uz.pdp.back.model;


import uz.pdp.back.model.enums.Role;

import java.io.Serializable;
import java.util.UUID;

public class User implements Serializable {
    private final UUID id = UUID.randomUUID();
    private String name;
    private int age;
    private String phone;
    private String password;
    private Role role;
    private Long balance;

}