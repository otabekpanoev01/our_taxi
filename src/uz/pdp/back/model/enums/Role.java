package uz.pdp.back.model.enums;

public enum Role {
    USER,
    DRIVER,
    ADMIN
}
