package uz.pdp.back.model.enums;

public enum CarType {
    START(100_000),
    COMFORT(300_000),
    BUSINESS(500_000);

    private final long pricePerSec;

    CarType(long pricePerSec) {
        this.pricePerSec = pricePerSec;
    }

    public long getPricePerSec() {
        return pricePerSec;
    }
}
