package uz.pdp.back.model;

import uz.pdp.back.payload.CarDto;

import java.io.Serializable;

public class Application implements Serializable {
    private int id;
    private boolean active = true;
    private CarDto car;
}
