package uz.pdp.back.model;

import uz.pdp.back.model.enums.CarType;

import java.io.Serializable;
import java.util.UUID;

public class Car implements Serializable {
    private final UUID id = UUID.randomUUID();
    private String name1;
    private User driver;
    private String number;
    private CarType type;
    private String balo;
}
